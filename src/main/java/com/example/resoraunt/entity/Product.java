package com.example.resoraunt.entity;

import com.example.resoraunt.entity.enums.ProductCondition;
import com.example.resoraunt.entity.enums.ProductType;
import lombok.*;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Table(name = "products")
@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "product_name")
    private String name;
    @Column(name = "amount")
    private Integer amount;
    @Column(name = "expiration_date")
    private Integer expirationDate;
    @OneToMany(mappedBy = "product")
    private List<Order> order;
    @Column(name = "expiration_date_begins")
    private Date expirationDateBegins;
    @Column(name = "expiration_date_ends")
    private Date expirationDateEnds;
    @Column(name = "product_type")
    private ProductType productType;
    @Column(name = "product_condition")
    @Builder.Default
    private ProductCondition productCondition = ProductCondition.PRODUCT_CONDITION_FRESH;
}





