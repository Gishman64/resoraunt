package com.example.resoraunt.service;

import com.example.resoraunt.entity.Product;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface ProductService {
    Page<Product> getAll(Pageable pageable);

    Product getById(Long id);

    Boolean create(Product product);

    Page<Product> getByStubName(String name, Pageable pageable);

    Product update(Product product, Long productId);

    Boolean deleteById(Long productId);

    Page<Product> getAllByName(final String name, Pageable pageable);
}
