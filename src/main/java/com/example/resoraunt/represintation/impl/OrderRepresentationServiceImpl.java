package com.example.resoraunt.represintation.impl;

import com.example.resoraunt.endpoint.dto.IdNameDto;
import com.example.resoraunt.endpoint.dto.OrderReqDto;
import com.example.resoraunt.endpoint.dto.OrderResDto;
import com.example.resoraunt.entity.Order;
import com.example.resoraunt.entity.Worker;
import com.example.resoraunt.entity.enums.OrderStatus;
import com.example.resoraunt.entity.enums.OrderType;
import com.example.resoraunt.represintation.OrderRepresentationService;
import com.example.resoraunt.service.OrderService;
import com.example.resoraunt.service.ProductService;
import com.example.resoraunt.service.WorkerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Objects;

@Service
public class OrderRepresentationServiceImpl implements OrderRepresentationService {

    private final OrderService orderService;
    private final ProductService productService;
    private final WorkerService workerService;

    @Autowired
    public OrderRepresentationServiceImpl(OrderService orderService,
                                          ProductService productService,
                                          WorkerService workerService) {
        this.orderService = orderService;
        this.productService = productService;
        this.workerService = workerService;
    }

    @Override
    public Page<OrderResDto> getAll(Pageable pageable) {
        return orderService.getAll(pageable)
                .map(this::toDto);
    }

    @Override
    public Page<OrderResDto> getAllByStatus(String orderStatus, Pageable pageable) {
        OrderStatus orderStatus1 = OrderStatus.valueOf(orderStatus);
        return orderService.getAllByStatus(orderStatus1, pageable)
                .map(this::toDto);
    }

    @Override
    public OrderResDto getById(Long orderId) {
        return toDto(orderService.getById(orderId));
    }

    @Override
    public OrderResDto createRequiredInFuture(OrderReqDto orderReqDto) {
        return toDto(orderService.createRequiredInFuture(toEntity(orderReqDto)));
    }

    @Override
    public OrderResDto createRequiredNow(OrderReqDto orderReqDto) {
        return toDto(orderService.createRequiredNow(toEntity(orderReqDto)));
    }

    @Override
    public OrderResDto assignee(Long orderId) {
        return toDto(orderService.assignee(orderId));
    }

    @Override
    public Boolean deleteById(Long orderId) {
        return orderService.deleteById(orderId);
    }

    @Override
    public OrderResDto update(Long orderId, Integer amount) {
        return toDto(orderService.update(orderId, amount));
    }

    @Override
    public OrderResDto completeTheRequiredInFuture(Long orderId) {
        return toDto(orderService.completeTheRequiredInFuture(orderId));
    }

    @Override
    public OrderResDto completeTheRequiredNow(Long orderId) {
        return toDto(orderService.completeTheRequiredNow(orderId));
    }

    @Override
    public Page<OrderResDto> getAllByType(String orderType, Pageable pageable) {
        OrderType type = OrderType.valueOf(orderType);
        return orderService.getAllByOrderType(type, pageable)
                .map(this::toDto);
    }

    private OrderResDto toDto(Order order) {
        OrderResDto newOrder = OrderResDto.builder()
                .amount(order.getAmount())
                .product(IdNameDto.builder()
                        .id(order.getProduct()
                                .getId())
                        .name(order.getProduct()
                                .getName())
                        .build())
                .id(order.getId())
                .orderStatus(order.getOrderStatus())
                .build();
        if (!Objects.isNull(order.getCustomer())) {
            newOrder.setCustomer(IdNameDto.builder()
                    .id(order.getCustomer()
                            .getId())
                    .name(order.getCustomer()
                            .getName())
                    .build());
        }
        if (!Objects.isNull(order.getProvider())) {
            newOrder.setProvider(IdNameDto.builder()
                    .id(order.getProvider().getId())
                    .name(order.getProvider()
                            .getUsername())
                    .build());
        }
        return newOrder;
    }

    private Order toEntity(OrderReqDto orderReqDto) {
        Worker dbCustomer = null;
        if (orderReqDto.getCustomerId() != null) {
            dbCustomer = workerService.getById(orderReqDto.getCustomerId());
        }

        return Order.builder()
                .amount(orderReqDto.getAmount())
                .orderType(orderReqDto.getOrderType())
                .product(productService
                        .getById(orderReqDto.getProductId()))
                .customer(dbCustomer)
                .build();
    }
}
