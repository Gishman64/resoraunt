package com.example.resoraunt.endpoint.dto;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class IdNameDto {
    private String name;
    private Long id;
}
