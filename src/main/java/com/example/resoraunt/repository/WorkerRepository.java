package com.example.resoraunt.repository;

import com.example.resoraunt.entity.Worker;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface WorkerRepository extends JpaRepository<Worker, Long> {
    Page<Worker> findAllByUsernameContainingIgnoreCase(String name, Pageable pageable);

    Worker findByUsername(String username);

    Worker findByName(String name);
}
