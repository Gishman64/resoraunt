package com.example.resoraunt.entity.enums;

public enum ProductCondition {
    PRODUCT_CONDITION_FRESH,
    PRODUCT_CONDITION_EXPIRES,
    PRODUCT_CONDITION_EXPIRED
}
