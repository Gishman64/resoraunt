package com.example.resoraunt.conf;

import com.example.resoraunt.entity.Worker;
import com.example.resoraunt.entity.enums.Role;
import com.example.resoraunt.repository.WorkerRepository;
import com.example.resoraunt.service.WorkerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Objects;

@Configuration
public class AdminConf {

    private final WorkerService workerService;

    private final WorkerRepository workerRepository;

    @Value("${default.admin.username}")
    String defaultAdminUsername;
    @Value("${default.admin.password}")
    String defaultAdminPass;

    @Autowired
    public AdminConf(WorkerService workerService, WorkerRepository workerRepository) {
        this.workerService = workerService;
        this.workerRepository = workerRepository;
    }

    @Bean
    public CommandLineRunner addAdmin() {
        return args -> {
            if (Objects.isNull(workerRepository.findByUsername(defaultAdminUsername))) {
                workerService.create(Worker.builder()
                        .username(defaultAdminUsername)
                        .name("admin")
                        .password(defaultAdminPass)
                        .enabled(true)
                        .role(Role.ROLE_ADMIN)
                        .build());
            }
        };


    }

}
