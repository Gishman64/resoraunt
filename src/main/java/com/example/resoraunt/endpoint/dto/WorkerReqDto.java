package com.example.resoraunt.endpoint.dto;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class WorkerReqDto {
    private String username;
    private String name;
    private String phone;
    private String password;
    private String role;

}
