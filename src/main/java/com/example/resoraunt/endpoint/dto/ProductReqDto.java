package com.example.resoraunt.endpoint.dto;

import com.example.resoraunt.entity.enums.ProductCondition;
import com.example.resoraunt.entity.enums.ProductType;
import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ProductReqDto {
    private String name;
    private Integer amount;
    private Integer expirationDate;
    private ProductType productType;
    private ProductCondition productCondition;
}
