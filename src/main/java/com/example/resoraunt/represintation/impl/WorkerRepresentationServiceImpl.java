package com.example.resoraunt.represintation.impl;

import com.example.resoraunt.endpoint.dto.WorkerReqDto;
import com.example.resoraunt.endpoint.dto.WorkerResDto;
import com.example.resoraunt.entity.Worker;
import com.example.resoraunt.entity.enums.Role;
import com.example.resoraunt.represintation.WorkerRepresentationService;
import com.example.resoraunt.service.WorkerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class WorkerRepresentationServiceImpl implements WorkerRepresentationService {

    private final WorkerService workerService;

    @Autowired
    public WorkerRepresentationServiceImpl(WorkerService workerService) {
        this.workerService = workerService;
    }

    @Override
    public Page<WorkerResDto> getAll(Pageable pageable) {
        return workerService.getAll(pageable)
                .map(this::toDto);
    }

    @Override
    public WorkerResDto getById(Long workerId) {
        return toDto(workerService.getById(workerId));
    }

    @Override
    public Page<WorkerResDto> getByStubName(String name, Pageable pageable) {
        return workerService.getByStubName(name, pageable).map(this::toDto);
    }

    @Override
    public WorkerResDto update(WorkerReqDto workerReqDto, Long workerId) {
        return toDto(workerService
                .update(toEntity(workerReqDto), workerId));
    }

    @Override
    public Boolean create(
            WorkerReqDto workerReqDto) {
        return workerService.create(toEntity(workerReqDto));
    }

    @Override
    public Boolean deleteById(Long workerId) {
        return workerService.deleteById(workerId);
    }

    private WorkerResDto toDto(Worker worker) {
        return WorkerResDto.builder()
                .id(worker.getId())
                .name(worker.getName())
                .username(worker.getUsername())
                .phone(worker.getPhone())
                .role(worker.getRole().getAuthority())
                .build();
    }

    private Worker toEntity(WorkerReqDto workerReqDto) {
        return Worker.builder()
                .username(workerReqDto.getUsername())
                .name(workerReqDto.getName())
                .phone(workerReqDto.getPhone())
                .password(workerReqDto.getPassword())
                .role(Role.valueOf(workerReqDto.getRole()))
                .build();
    }
}


