package com.example.resoraunt.repository;

import com.example.resoraunt.entity.Order;
import com.example.resoraunt.entity.enums.OrderStatus;
import com.example.resoraunt.entity.enums.OrderType;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface OrderRepository extends JpaRepository<Order, Long> {
    Page<Order> findAllByProviderIdOrderByOrderStatus(Long workerId, Pageable pageable);

    Page<Order> findAllByOrderStatusOrderByProvider(OrderStatus orderStatus, Pageable pageable);

    List<Order> findAllByProductNameContainingIgnoreCase(String name, Pageable pageable);

    Page<Order> findAllByOrderType(OrderType orderType, Pageable pageable);
}
