package com.example.resoraunt.endpoint.rest;

import com.example.resoraunt.endpoint.dto.OrderReqDto;
import com.example.resoraunt.endpoint.dto.OrderResDto;
import com.example.resoraunt.represintation.OrderRepresentationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/order")
public class OrderController {

    private final OrderRepresentationService orderRepresentationService;

    @Autowired
    public OrderController(OrderRepresentationService orderRepresentationService) {
        this.orderRepresentationService = orderRepresentationService;
    }

    @GetMapping(path = "/{id}")
    public OrderResDto getById(@PathVariable(value = "id") Long orderId) {
        return orderRepresentationService.getById(orderId);
    }

    @GetMapping
    public Page<OrderResDto> getAll(Pageable pageable) {
        return orderRepresentationService.getAll(pageable);
    }

    @GetMapping("/filter/status")
    public Page<OrderResDto> getAllByStatus(@RequestParam(value = "status") String orderStatus,
                                            Pageable pageable) {
        return orderRepresentationService.getAllByStatus(orderStatus, pageable);
    }

    @GetMapping("/filter/type")
    public Page<OrderResDto> getAllByOrderType(@RequestParam("orderType") String orderType,
                                               Pageable pageable) {
        return orderRepresentationService.getAllByType(orderType, pageable);
    }

    @PostMapping("/future")
    @PreAuthorize("!hasAuthority('ROLE_PROVIDER')")
    public OrderResDto createRequiredInFuture(@RequestBody OrderReqDto orderReqDto) {
        return orderRepresentationService.createRequiredInFuture(orderReqDto);
    }

    @PostMapping("/now")
    @PreAuthorize("!hasAuthority('ROLE_PROVIDER')")
    public OrderResDto createRequiredNow(@RequestBody OrderReqDto orderReqDto) {
        return orderRepresentationService.createRequiredNow(orderReqDto);
    }

    @PatchMapping("/{id}")
    @PreAuthorize("!hasAuthority('ROLE_PROVIDER')")
    public OrderResDto update(@PathVariable(value = "id") Long orderId,
                              @RequestParam(value = "amount") Integer amount) {
        return orderRepresentationService.update(orderId, amount);
    }

    @PatchMapping("/assignee/{id}")
    @PreAuthorize("hasAuthority('ROLE_PROVIDER')")
    public OrderResDto assignee(@PathVariable(value = "id") Long orderId) {
        return orderRepresentationService.assignee(orderId);
    }


    @PatchMapping("/complete/future/{id}")
    public OrderResDto completeTheRequiredInFuture(@PathVariable(value = "id") Long orderId) {
        return orderRepresentationService.completeTheRequiredInFuture(orderId);
    }

    @PatchMapping("/complete/now/{id}")
    @PreAuthorize("!hasAuthority('ROLE_PROVIDER')")
    public OrderResDto completeTheRequiredNow(@PathVariable(value = "id") Long orderId) {
        return orderRepresentationService.completeTheRequiredNow(orderId);
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasAnyAuthority('ROLE_ADMIN','ROLE_MANAGER')")
    public Boolean deleteById(@PathVariable(value = "id") Long orderId) {
        return orderRepresentationService.deleteById(orderId);
    }


}
