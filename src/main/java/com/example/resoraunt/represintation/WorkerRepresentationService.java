package com.example.resoraunt.represintation;

import com.example.resoraunt.endpoint.dto.WorkerReqDto;
import com.example.resoraunt.endpoint.dto.WorkerResDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public interface WorkerRepresentationService {
    Page<WorkerResDto> getAll(Pageable pageable);

    WorkerResDto getById(Long workerId);

    Page<WorkerResDto> getByStubName(String name, Pageable pageable);

    WorkerResDto update(WorkerReqDto workerReqDto, Long workerId);

    Boolean create(WorkerReqDto workerReqDto);

    Boolean deleteById(Long workerId);
}
