package com.example.resoraunt.endpoint.dto;

import com.example.resoraunt.entity.enums.OrderStatus;
import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class OrderResDto {
    private Long id;
    private IdNameDto product;
    private IdNameDto provider;
    private Integer amount;
    private OrderStatus orderStatus;
    private IdNameDto customer;
}
