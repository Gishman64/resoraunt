package com.example.resoraunt.represintation;

import com.example.resoraunt.endpoint.dto.ProductReqDto;
import com.example.resoraunt.endpoint.dto.ProductResDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public interface ProductRepresentationService {

    Page<ProductResDto> getAll(Pageable pageable);

    Page<ProductResDto> getAllByName(final String name, Pageable pageable);

    Page<ProductResDto> getByStubName(final String name, Pageable pageable);

    ProductResDto getById(Long productId);

    ProductResDto update(ProductReqDto productReqDto, Long productId);

    Boolean create(ProductReqDto productReqDto);

    Boolean deleteById(Long productId);

}
