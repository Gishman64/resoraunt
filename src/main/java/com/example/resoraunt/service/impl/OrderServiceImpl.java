package com.example.resoraunt.service.impl;

import com.example.resoraunt.entity.Order;
import com.example.resoraunt.entity.Product;
import com.example.resoraunt.entity.User;
import com.example.resoraunt.entity.Worker;
import com.example.resoraunt.entity.enums.OrderStatus;
import com.example.resoraunt.entity.enums.OrderType;
import com.example.resoraunt.entity.enums.ProductCondition;
import com.example.resoraunt.repository.OrderRepository;
import com.example.resoraunt.repository.ProductRepository;
import com.example.resoraunt.repository.WorkerRepository;
import com.example.resoraunt.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

@Service
public class OrderServiceImpl implements OrderService {

    private final OrderRepository orderRepository;

    private final ProductRepository productRepository;

    private final WorkerRepository workerRepository;

    @Autowired
    public OrderServiceImpl(OrderRepository orderRepository,
                            ProductRepository productRepository, WorkerRepository workerRepository) {
        this.orderRepository = orderRepository;
        this.productRepository = productRepository;
        this.workerRepository = workerRepository;
    }

    @Transactional
    @Override
    public Page<Order> getAll(Pageable pageable) {
        return orderRepository.findAll(pageable);
    }

    @Transactional
    @Override
    public Page<Order> getAllByOrderType(OrderType orderType, Pageable pageable) {
        return orderRepository.findAllByOrderType(orderType, pageable);
    }

    @Transactional
    @Override
    public Boolean deleteById(Long orderId) {

        orderRepository.deleteById(orderId);
        return true;
    }

    @Transactional
    @Override
    public Order getById(Long orderId) {
        return orderRepository.getOne(orderId);
    }

    @Transactional
    @Override
    public Page<Order> getAllByStatus(OrderStatus orderStatus, Pageable pageable) {
        return orderRepository.findAllByOrderStatusOrderByProvider(orderStatus, pageable);
    }

    @Transactional
    @Override
    public Order createRequiredInFuture(Order orderReq) {
        Order newOrder = Order.builder()
                .product(orderReq.getProduct())
                .amount(orderReq.getAmount())
                .orderStatus(OrderStatus.STATUS_AWAITING)
                .orderType(orderReq.getOrderType())
                .build();
        return orderRepository.save(newOrder);
    }

    @Override
    public Order createRequiredNow(Order order) {
        Order newOrder = Order.builder()
                .product(order.getProduct())
                .amount(order.getAmount())
                .orderStatus(OrderStatus.STATUS_IN_PROCESSING)
                .orderType(order.getOrderType())
                .customer(order.getCustomer())
                .build();
        return orderRepository.save(newOrder);
    }

    @Transactional
    @Override
    public Order assignee(Long orderId) {
        Order dbOrder = orderRepository.getOne(orderId);
        User auth = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Worker dbProvider = workerRepository.findByUsername(auth.getUsername());
        if (null != dbOrder.getProvider()) {
            throw new IllegalStateException("заказ уже назначен");
        }
        dbOrder.setProvider(dbProvider);
        dbOrder.setOrderStatus(OrderStatus.STATUS_IN_PROCESSING);
        return dbOrder;
    }

    @Transactional
    @Override
    public Order update(Long orderId, Integer amount) {
        Order dbOrder = orderRepository.getOne(orderId);
        dbOrder.setAmount(amount);
        return dbOrder;
    }

    @Transactional
    @Override
    public Order completeTheRequiredInFuture(Long orderId) {
        Order dbOrder = orderRepository.getOne(orderId);
        Product dbProduct = dbOrder.getProduct();
        if (dbOrder.getOrderStatus().equals(OrderStatus.STATUS_IS_DONE)) {
            throw new IllegalStateException("уже завершено");
        }

        dbOrder.setOrderStatus(OrderStatus.STATUS_IS_DONE);
        Product newPosition = Product.builder().name(dbProduct.getName())
                .amount(dbOrder.getAmount())
                .productType(dbProduct.getProductType())
                .productCondition(ProductCondition.PRODUCT_CONDITION_FRESH)
                .expirationDate(dbProduct.getExpirationDate())
                .expirationDateBegins(new Date())
                .expirationDateEnds(new Date(dbProduct.getExpirationDate()
                        * 86_400_400L
                        + System.currentTimeMillis()))
                .build();
        productRepository.save(newPosition);
        return dbOrder;
    }

    @Transactional
    @Override
    public Order completeTheRequiredNow(Long orderId) {
        Order dbOrder = orderRepository.getOne(orderId);

        if (dbOrder.getOrderStatus().equals(OrderStatus.STATUS_IS_DONE)) {
            throw new IllegalStateException("уже завершено");
        }

        Product dbProduct = dbOrder.getProduct();
        dbOrder.setOrderStatus(OrderStatus.STATUS_IS_DONE);
        Integer oldProductAmount = dbProduct.getAmount();
        Integer requiredAmount = dbOrder.getAmount();
        dbProduct.setAmount(oldProductAmount - requiredAmount);

        return dbOrder;
    }
}
