package com.example.resoraunt.entity.enums;

public enum OrderType {
    TYPE_REQUIRED_NOW,
    TYPE_REQUIRED_IN_FUTURE
}
