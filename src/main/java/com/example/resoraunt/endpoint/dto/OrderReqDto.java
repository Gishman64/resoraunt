package com.example.resoraunt.endpoint.dto;

import com.example.resoraunt.entity.enums.OrderType;
import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class OrderReqDto {
    private OrderType orderType;
    private Integer amount;
    private Long productId;
    private Long customerId;
}
