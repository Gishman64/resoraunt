package com.example.resoraunt.entity;

import com.example.resoraunt.entity.enums.OrderStatus;
import com.example.resoraunt.entity.enums.OrderType;
import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "orders")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Order {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "order_status")
    private OrderStatus orderStatus;
    @Column(name = "order_type")
    private OrderType orderType;
    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "provider_id")
    private Worker provider;
    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "worker_id")
    private Worker customer;
    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "product_id")
    private Product product;
    @Column(name = "amount")
    private Integer amount;
}
