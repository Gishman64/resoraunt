package com.example.resoraunt.entity.enums;

public enum ProductType {
    GRAIN_GOODS,
    FRUITS_VEGETABLES,
    PASTRY,
    MEAT,
    SEAFOOD,
    DAIRY,
    EGGS,
    FLAVORING_GOODS
}
