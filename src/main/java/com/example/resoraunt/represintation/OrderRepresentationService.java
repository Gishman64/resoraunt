package com.example.resoraunt.represintation;

import com.example.resoraunt.endpoint.dto.OrderReqDto;
import com.example.resoraunt.endpoint.dto.OrderResDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public interface OrderRepresentationService {

    Page<OrderResDto> getAll(Pageable pageable);

    OrderResDto createRequiredInFuture(OrderReqDto orderReqDto);

    OrderResDto createRequiredNow(OrderReqDto orderReqDto);

    OrderResDto assignee(Long orderId);

    Page<OrderResDto> getAllByStatus(String orderStatus, Pageable pageable);

    OrderResDto getById(Long orderId);

    Boolean deleteById(Long orderId);

    OrderResDto update(Long orderId, Integer amount);

    OrderResDto completeTheRequiredInFuture(Long orderId);

    OrderResDto completeTheRequiredNow(Long orderId);

    Page<OrderResDto> getAllByType(String orderType, Pageable pageable);
}
