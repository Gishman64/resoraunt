package com.example.resoraunt.represintation.impl;

import com.example.resoraunt.endpoint.dto.ProductReqDto;
import com.example.resoraunt.endpoint.dto.ProductResDto;
import com.example.resoraunt.entity.Product;
import com.example.resoraunt.represintation.ProductRepresentationService;
import com.example.resoraunt.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class ProductRepresentationServiceImpl implements ProductRepresentationService {

    private final ProductService productService;

    @Autowired
    public ProductRepresentationServiceImpl(ProductService productService) {
        this.productService = productService;
    }

    @Override
    public Page<ProductResDto> getAll(
            Pageable pageable) {
        return productService.getAll(pageable)
                .map(this::toDto);
    }

    @Override
    public Page<ProductResDto> getAllByName(String name, Pageable pageable) {
        return productService.getAllByName(name, pageable)
                .map(this::toDto);

    }

    @Override
    public Page<ProductResDto> getByStubName(String name, Pageable pageable) {
        return productService.getByStubName(name, pageable)
                .map(this::toDto);
    }

    @Override
    public ProductResDto getById(Long productId) {
        return toDto(productService.getById(productId));
    }

    @Override
    public ProductResDto update(ProductReqDto productReqDto, Long productId) {
        return toDto(productService
                .update(
                        toEntity(productReqDto),
                        productId));
    }

    @Override
    public Boolean create(ProductReqDto productReqDto) {
        return productService
                .create(toEntity(productReqDto));
    }

    @Override
    public Boolean deleteById(Long productId) {
        return productService
                .deleteById(productId);
    }

    private ProductResDto toDto(Product product) {
        return ProductResDto.builder()
                .amount(product.getAmount())
                .name(product.getName())
                .id(product.getId())
                .expirationDateBegins(product.getExpirationDateBegins()
                        .toString())
                .expirationDateEnds(product.getExpirationDateEnds()
                        .toString())
                .productType(product.getProductType())
                .productCondition(product.getProductCondition())
                .build();
    }

    private Product toEntity(ProductReqDto productReqDto) {
        return Product.builder()
                .name(productReqDto.getName())
                .amount(productReqDto.getAmount())
                .productType(productReqDto.getProductType())
                .expirationDate(productReqDto.getExpirationDate())
                .build();
    }
}

