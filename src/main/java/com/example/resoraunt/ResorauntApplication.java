package com.example.resoraunt;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ResorauntApplication {

    public static void main(String[] args) {
        SpringApplication.run(ResorauntApplication.class, args);
    }

}
