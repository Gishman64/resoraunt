package com.example.resoraunt.endpoint.rest;

import com.example.resoraunt.endpoint.dto.ProductReqDto;
import com.example.resoraunt.endpoint.dto.ProductResDto;
import com.example.resoraunt.represintation.ProductRepresentationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/product")
@PreAuthorize("!hasAnyAuthority('PROVIDER')")
public class ProductController {

    private final ProductRepresentationService productRepresentationService;

    @Autowired
    public ProductController(ProductRepresentationService productRepresentationService) {
        this.productRepresentationService = productRepresentationService;
    }

    @GetMapping
    public Page<ProductResDto> getAll(Pageable pageable) {
        return productRepresentationService.getAll(pageable);
    }

    @GetMapping("/{id}")
    public ProductResDto getById(@PathVariable(value = "id") Long productId) {
        return productRepresentationService.getById(productId);
    }

    @GetMapping("/suggest")
    public Page<ProductResDto> getByStubName(@RequestParam(value = "name") String name,
                                             Pageable pageable) {
        return productRepresentationService.getByStubName(name, pageable);
    }

    @GetMapping("/name")
    public Page<ProductResDto> getAllByName(@RequestParam(value = "name") String name,
                                            Pageable pageable) {
        return productRepresentationService.getAllByName(name, pageable);
    }

    @PostMapping
    public Boolean create(@RequestBody ProductReqDto productReqDto) {
        return productRepresentationService.create(productReqDto);
    }

    @PatchMapping(value = "/{id}")
    public ProductResDto update(@PathVariable(value = "id") Long productId,
                                @RequestBody ProductReqDto productReqDto) {
        return productRepresentationService.update(productReqDto, productId);
    }

    @DeleteMapping("/{id}")
    public Boolean deleteById(@PathVariable(value = "id") Long productId) {
        return productRepresentationService.deleteById(productId);
    }
}
