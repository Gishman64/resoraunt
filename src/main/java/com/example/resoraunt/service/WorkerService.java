package com.example.resoraunt.service;

import com.example.resoraunt.entity.Worker;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.userdetails.UserDetailsService;

public interface WorkerService extends UserDetailsService {
    Page<Worker> getAll(Pageable pageable);

    Worker getById(Long workerId);

    Boolean create(Worker worker);

    Page<Worker> getByStubName(String name, Pageable pageable);

    Worker getByUsername(String name);

    Worker update(Worker worker, Long workerId);

    Boolean deleteById(Long workerId);
}
