package com.example.resoraunt.endpoint.dto;

import com.example.resoraunt.entity.enums.ProductCondition;
import com.example.resoraunt.entity.enums.ProductType;
import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ProductResDto {
    private String name;
    private Long id;
    private Integer amount;
    private String expirationDateBegins;
    private String expirationDateEnds;
    private ProductType productType;
    private ProductCondition productCondition;
}
