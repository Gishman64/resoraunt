package com.example.resoraunt.endpoint.dto;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class WorkerResDto {
    private Long id;
    private String username;
    private String name;
    private String phone;
    private String role;
}
