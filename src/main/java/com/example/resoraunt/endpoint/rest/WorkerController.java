package com.example.resoraunt.endpoint.rest;

import com.example.resoraunt.endpoint.dto.WorkerReqDto;
import com.example.resoraunt.endpoint.dto.WorkerResDto;
import com.example.resoraunt.represintation.WorkerRepresentationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/worker")
@PreAuthorize("hasAnyAuthority('ROLE_MANAGER','ROLE_ADMIN')")
public class WorkerController {

    private final WorkerRepresentationService workerRepresentationService;

    @Autowired
    public WorkerController(WorkerRepresentationService workerRepresentationService) {
        this.workerRepresentationService = workerRepresentationService;
    }

    @GetMapping
    public Page<WorkerResDto> getAll(Pageable pageable) {
        return workerRepresentationService.getAll(pageable);
    }

    @GetMapping(value = "/{id}")
    public WorkerResDto getById(@PathVariable(value = "id") Long workerId) {
        return workerRepresentationService.getById(workerId);
    }

    @GetMapping(value = "/suggest")
    public Page<WorkerResDto> getByStubName(@RequestParam(value = "name") String name,
                                            Pageable pageable) {
        return workerRepresentationService.getByStubName(name, pageable);
    }

    @PostMapping
    public Boolean create(@RequestBody WorkerReqDto workerReqDto) {
        return workerRepresentationService.create(workerReqDto);
    }

    @PatchMapping(value = "/{id}")
    public WorkerResDto update(@PathVariable(value = "id") Long workerId,
                               @RequestBody WorkerReqDto workerReqDto) {
        return workerRepresentationService.update(workerReqDto, workerId);
    }

    @DeleteMapping(value = "/{id}")
    public Boolean deleteById(@PathVariable(value = "id") Long workerId) {
        return workerRepresentationService.deleteById(workerId);
    }
}
