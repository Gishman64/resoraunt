package com.example.resoraunt.service.impl;

import com.example.resoraunt.entity.User;
import com.example.resoraunt.entity.Worker;
import com.example.resoraunt.repository.WorkerRepository;
import com.example.resoraunt.service.WorkerService;
import com.example.resoraunt.utills.CustomBeanUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;

@Service
public class WorkerServiceImpl implements WorkerService {

    private final WorkerRepository workerRepository;

    @Autowired
    public WorkerServiceImpl(WorkerRepository workerRepository) {
        this.workerRepository = workerRepository;
    }

    @Transactional
    @Override
    public Page<Worker> getAll(Pageable pageable) {
        return workerRepository.findAll(pageable);

    }

    @Transactional
    @Override
    public Worker getById(Long workerId) {
        return workerRepository.getOne(workerId);
    }

    @Transactional
    @Override
    public Boolean create(Worker worker) {
        PasswordEncoder passwordEncoder = new BCryptPasswordEncoder(10);
        Worker target = Worker.builder()
                .username(worker.getUsername())
                .phone(worker.getPhone())
                .name(worker.getName())
                .password(passwordEncoder
                        .encode(worker.getPassword()))
                .role(worker.getRole())
                .build();
        workerRepository.save(target);
        return true;
    }

    @Transactional
    @Override
    public Page<Worker> getByStubName(String name, Pageable pageable) {
        return workerRepository
                .findAllByUsernameContainingIgnoreCase(name, pageable);
    }

    @Transactional
    @Override
    public Worker getByUsername(String name) {
        return workerRepository.findByUsername(name);
    }


    @Transactional
    @Override
    public Worker update(Worker worker, Long workerId) {
        Worker dbWorker = workerRepository.getOne(workerId);
        BeanUtils.copyProperties(worker,
                dbWorker,
                CustomBeanUtils.getNullPropertyNames(worker));
        return dbWorker;
    }

    @Transactional
    @Override
    public Boolean deleteById(Long workerId) {
        workerRepository.deleteById(workerId);
        return true;
    }

    @Transactional
    @Override
    public UserDetails loadUserByUsername(String s)
            throws UsernameNotFoundException {
        Worker dbWorker = workerRepository.findByUsername(s);
        return User.builder()
                .password(dbWorker.getPassword())
                .username(dbWorker.getUsername())
                .authorities(
                        Collections.singletonList(dbWorker.getRole())
                )
                .build();
    }
}
