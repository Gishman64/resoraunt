package com.example.resoraunt.service;

import com.example.resoraunt.entity.Order;
import com.example.resoraunt.entity.enums.OrderStatus;
import com.example.resoraunt.entity.enums.OrderType;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface OrderService {

    Page<Order> getAll(Pageable pageable);

    Order createRequiredInFuture(Order order);

    Order createRequiredNow(Order order);

    Page<Order> getAllByOrderType(OrderType orderType, Pageable pageable);

    Boolean deleteById(Long orderId);

    Order getById(Long orderId);

    Page<Order> getAllByStatus(OrderStatus orderStatus, Pageable pageable);

    Order assignee(Long orderId);

    Order update(Long orderId, Integer amount);

    Order completeTheRequiredInFuture(Long orderId);

    Order completeTheRequiredNow(Long orderId);
}
