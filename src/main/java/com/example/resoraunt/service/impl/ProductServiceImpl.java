package com.example.resoraunt.service.impl;

import com.example.resoraunt.entity.Product;
import com.example.resoraunt.entity.enums.ProductCondition;
import com.example.resoraunt.repository.ProductRepository;
import com.example.resoraunt.service.ProductService;
import com.example.resoraunt.utills.CustomBeanUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.concurrent.TimeUnit;

@Service
public class ProductServiceImpl implements ProductService {

    private final ProductRepository productRepository;


    @Autowired
    public ProductServiceImpl(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @Transactional
    @Override
    public Page<Product> getAll(Pageable pageable) {
        return productRepository.findAll(pageable)
                .map(this::updateCondition);
    }

    @Transactional
    @Override
    public Page<Product> getByStubName(String name, Pageable pageable) {
        return productRepository
                .findAllByNameContainingIgnoreCase(name, pageable)
                .map(this::updateCondition);
    }

    @Transactional
    @Override
    public Product getById(Long id) {
        return updateCondition(productRepository.findById(id)
                .orElseThrow(() -> new IllegalStateException("not found")));
    }

    @Transactional
    @Override
    public Boolean create(Product product) {
        Product target = Product.builder()
                .name(product.getName())
                .amount(product.getAmount())
                .productType(product.getProductType())
                .expirationDate(product.getExpirationDate())
                .expirationDateBegins(new Date())
                .expirationDateEnds(new Date(product.getExpirationDate()
                        * 86_400_000L
                        + System.currentTimeMillis()))
                .build();

        productRepository.save(target);
        return true;
    }

    @Transactional
    @Override
    public Product update(Product product, Long productId) {
        Product dbProduct = productRepository.getOne(productId);
        BeanUtils.copyProperties(product,
                dbProduct,
                CustomBeanUtils.getNullPropertyNames(product));

        return dbProduct;
    }

    @Transactional
    @Override
    public Boolean deleteById(Long productId) {
        productRepository.deleteById(productId);

        return true;
    }

    @Transactional
    @Override
    public Page<Product> getAllByName(String name, Pageable pageable) {
        return productRepository.findAllByName(name, pageable)
                .map(this::updateCondition);
    }

    private Product updateCondition(Product product) {
        Long current = new Date().getTime();
        Long expirationEnds;
        long tempExpirationDays;
        TimeUnit t = TimeUnit.MILLISECONDS;
        expirationEnds = product.getExpirationDateEnds()
                .getTime();
        tempExpirationDays = t.toDays(expirationEnds - current);
        if (tempExpirationDays <= 0) {
            product.setProductCondition(ProductCondition.PRODUCT_CONDITION_EXPIRED);
        } else {
            product.setProductCondition(tempExpirationDays <= 2 ?
                    ProductCondition.PRODUCT_CONDITION_EXPIRES
                    : product.getProductCondition());
        }

        return product;
    }

}
