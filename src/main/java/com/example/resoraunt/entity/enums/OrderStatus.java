package com.example.resoraunt.entity.enums;

public enum OrderStatus {
    STATUS_AWAITING,
    STATUS_IN_PROCESSING,
    STATUS_IS_DONE
}
